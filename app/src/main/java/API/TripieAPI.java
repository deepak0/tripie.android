package API;

import model.ItineraryPOJO.Main;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by deepak on 26/12/15.
 */
public interface TripieAPI {
    @GET("/api/Plan/")
    Call<Main> getItinerary(@Query("City") String City,
                            @Query("deviceId") String deviceId,
                            @Query("StartTime") String StartTime,
                            @Query("EndTime") String EndTime,
                            @Query("StartDate") String StartDate,
                            @Query("EndDate") String EndDate,
                            @Query("Visited") String Visited,
                            @Query("Interest") String Interest,
                            @Query("StartLocation") String StartLocation,
                            @Query("StayLocation") String StayLocation);
}
