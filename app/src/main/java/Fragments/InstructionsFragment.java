package Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tripie.app.tripie.R;

/**
 * Created by deepak on 25/12/15.
 */
public class InstructionsFragment extends Fragment {
    private Integer mPage;
    private String layoutImage;
    private ImageView mImage;

    public static Fragment newInstance(){
        return new InstructionsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.instructions, container, false);
        Bundle args = getArguments();
        mPage = args.getInt("page");
        layoutImage = "inst" + mPage;
        mImage = (ImageView)v.findViewById(R.id.instruction_image);
        mImage.setBackgroundResource(getResources().getIdentifier(
                layoutImage, "drawable", getContext().getPackageName()));
        return v;
    }
}
