package Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tripie.app.tripie.BasicInfoActivity;
import com.tripie.app.tripie.JSONParser;
import com.tripie.app.tripie.R;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by deepak on 25/12/15.
 */
public class SignUpInstruction extends Fragment {
    private Button mSignUp;
    private EditText mPhoneContact;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private String mDeviceId;
    private Context mContext;

    public static Fragment newInstance(){
        return new SignUpInstruction();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sign_up, container, false);
        mContext = this.getContext();

        mSharedPreferences = mContext.getSharedPreferences(
                getString(R.string.tripie_sign_up_key), Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();

        mDeviceId = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);

        mPhoneContact = (EditText)v.findViewById(R.id.phone_number);
        mSignUp = (Button)v.findViewById(R.id.sign_up_button);
        mSignUp.setOnClickListener(mSignUpListener);

        return v;
    }

    private View.OnClickListener mSignUpListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AsyncTask sign_up_request = new SignUpRequest();
            String phone = mPhoneContact.getText().toString();
            if (phone.length() == 10){
                String sign_up_url = "http://tripie.in/api/signup/?mob="+phone+"&deviceId="+mDeviceId;
                sign_up_request.execute(sign_up_url);
            } else {
                Toast.makeText(mContext, "Enter Correct Phone Number", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private class SignUpRequest extends AsyncTask {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(mContext);
            pd.setMessage("Signing Up");
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                JSONParser jParser = new JSONParser();
                return jParser.getJSONFromUrl(params[0].toString());
            }catch (IOException e){
                Log.e("JSON_ERROR", "Unable to retrieve content", e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object json) {
            try {
                pd.hide();
                pd.dismiss();
                JSONObject jsonObject = new JSONObject(json.toString());
                mEditor.putString(getString(R.string.tripie_sign_up_key), jsonObject.get("key").toString());
                mEditor.commit();
                Intent i = new Intent(mContext, BasicInfoActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            } catch (Exception e){
                Log.e("JSON_ERROR", "Could not parse malformed JSON");
                Toast.makeText(mContext, "Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
