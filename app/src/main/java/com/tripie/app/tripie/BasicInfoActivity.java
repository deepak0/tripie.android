package com.tripie.app.tripie;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import model.Data;

/**
 * Created by deepak on 12/4/15.
 */
public class BasicInfoActivity extends Activity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{
    private Data mData;
    private Spinner mLocationSpinner;
    private Button mNext;
    private EditText mStartDay;
    private EditText mStartTime;
    private EditText mEndDay;
    private EditText mEndTime;
    private Integer mWhichDate;
    private Integer mWhichTime;
    private CheckBox mBoxIa;
    private CheckBox mBoxIb;
    private CheckBox mBoxIc;
    private CheckBox mBoxId;
    private CheckBox mBoxIe;
    private String mInterests;

    @Override
    public void onCreate(Bundle savedInstanceState){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_info);
        /**
         * Custom Action Bar */
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);
        mActionBar.setDisplayShowCustomEnabled(true);

        mData = Data.getInstance(this);
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, mData.getPROJECT_TOKEN());

        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        mData.setDeviceId(android_id);

        try {
            JSONObject props = new JSONObject();
            props.put("DeviceId", android_id);
            props.put("Activity", "InfoActivity");
            mixpanel.track("Logged In - Main Activity Started", props);
        } catch (JSONException e) {
            Log.e("MIXPANEL", "Unable to add properties to JSONObject", e);
        }

        mLocationSpinner = (Spinner)findViewById(R.id.location_spinner);
        mNext = (Button)findViewById(R.id.next_button);
        mStartDay = (EditText)findViewById(R.id.start_date_picker);
        mStartTime = (EditText)findViewById(R.id.start_time_picker);
        mEndDay = (EditText)findViewById(R.id.end_date_picker);
        mEndTime = (EditText)findViewById(R.id.end_time_picker);
        mBoxIa = (CheckBox)findViewById(R.id.interest_site_seeing);
        mBoxIb = (CheckBox)findViewById(R.id.interest_religious);
        mBoxIe = (CheckBox)findViewById(R.id.interest_shopping);
        mBoxIc = (CheckBox)findViewById(R.id.interest_amusement);
        mBoxId = (CheckBox)findViewById(R.id.interest_night_life);

        mLocationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mData.setLocation(mLocationSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mData.setLocation("Mumbai");
            }
        });

        mStartDay.setFocusable(false);
        mEndDay.setFocusable(false);
        mStartTime.setFocusable(false);
        mEndTime.setFocusable(false);
        mStartDay.setOnTouchListener(mDayClicked);
        mEndDay.setOnTouchListener(mDayClicked);
        mStartTime.setOnTouchListener(mTimeClicked);
        mEndTime.setOnTouchListener(mTimeClicked);
        mNext.setOnClickListener(mNextClicked);
    }

    private View.OnClickListener mNextClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mStartDay.getText().toString().equals("")
                    || mEndDay.getText().toString().equals("")){
                Toast t = Toast.makeText(v.getContext(), "Enter date fields", Toast.LENGTH_SHORT);
                t.show();
            } else {
                setStartTime();
                //String days = calculateDays(mStartDay.getText().toString(), mEndDay.getText().toString()).toString();
                mInterests = ((mBoxIa.isChecked()) ? 1 : 0)
                        + "," + ((mBoxIb.isChecked()) ? 1 : 0)
                        + "," + ((mBoxIc.isChecked()) ? 1 : 0)
                        + "," + ((mBoxId.isChecked()) ? 1 : 0)
                        + "," + ((mBoxIe.isChecked()) ? 1 : 0);
                mData.setInterests(mInterests);
                Intent i = new Intent(BasicInfoActivity.this, PlanActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        }
    };

    private View.OnTouchListener mDayClicked = new View.OnTouchListener(){
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            switch (event.getAction()){
                case MotionEvent.ACTION_UP:
                    mWhichDate = v.getId();
                    Calendar mCalendar = Calendar.getInstance();
                    DatePickerDialog mDpd = DatePickerDialog.newInstance(
                            BasicInfoActivity.this,
                            mCalendar.get(Calendar.YEAR),
                            mCalendar.get(Calendar.MONTH),
                            mCalendar.get(Calendar.DAY_OF_MONTH)
                    );
                    mDpd.setMinDate(mCalendar);
                    mDpd.show(getFragmentManager(), "DatePickerDialog");
                    break;
                case MotionEvent.ACTION_DOWN:
                    break;
                default:
                    break;
            }
            return true;
        }
    };

    private View.OnTouchListener mTimeClicked = new View.OnTouchListener(){
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            switch (event.getAction()){
                case MotionEvent.ACTION_UP:
                    mWhichTime = v.getId();
                    Calendar mCalendar = Calendar.getInstance();
                    TimePickerDialog mTpd = TimePickerDialog.newInstance(
                            BasicInfoActivity.this,
                            mCalendar.get(Calendar.HOUR_OF_DAY),
                            mCalendar.get(Calendar.MINUTE),
                            false
                    );
                    mTpd.setMinTime(5,0,0);
                    mTpd.show(getFragmentManager(), "TimePickerDialog");
                    break;
                case MotionEvent.ACTION_DOWN:
                    break;
                default:
                    break;
            }
            return true;
        }
    };

    private void setStartTime(){
        Calendar current = Calendar.getInstance();
        int current_day = current.get(Calendar.DAY_OF_MONTH);
        int selected_day = Integer.parseInt(mData.getStartDate().substring(0, 2));
        Boolean is_same_day = (current_day==selected_day);
        Log.d("DAY", "Current: "+current_day+" Selected: "+selected_day+" result is same: "+is_same_day.toString());
        if (mStartTime.getText().toString().equals("") && is_same_day){
            int hour = current.get(Calendar.HOUR_OF_DAY);
            int min = current.get(Calendar.MINUTE);
            int time = (hour-5)*60+min;
            time = (time>240) ? time : 240;
            mData.setStartTime(String.valueOf(time));
        }
    }

    private Integer calculateDays(String Created_date_String, String Expire_date_String) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        Date Created_convertedDate=null,Expire_CovertedDate=null,todayWithZeroTime=null;
        try
        {
            Created_convertedDate = dateFormat.parse(Created_date_String);
            Expire_CovertedDate = dateFormat.parse(Expire_date_String);

            Date today = new Date();

            todayWithZeroTime =dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e)
        {
            e.printStackTrace();
        }


        int c_year=0,c_month=0,c_day=0;

        if(Created_convertedDate.after(todayWithZeroTime))
        {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(Created_convertedDate);

            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);

        }
        else
        {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(todayWithZeroTime);

            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);
        }

        Calendar e_cal = Calendar.getInstance();
        e_cal.setTime(Expire_CovertedDate);

        int e_year = e_cal.get(Calendar.YEAR);
        int e_month = e_cal.get(Calendar.MONTH);
        int e_day = e_cal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(c_year, c_month, c_day);
        date2.clear();
        date2.set(e_year, e_month, e_day);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return (int)dayCount;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth){
        String selected_day = dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
        String custom_day;
        if (dayOfMonth<10 && monthOfYear<9) {
            custom_day = "0"+dayOfMonth+"0"+(monthOfYear+1)+year;
        } else if (dayOfMonth<10 && monthOfYear>=9){
            custom_day = "0"+dayOfMonth+(monthOfYear+1)+year;
        } else if (dayOfMonth>=10 && monthOfYear<9){
            custom_day = dayOfMonth+"0"+(monthOfYear+1)+year;
        } else {
            custom_day = dayOfMonth+String.valueOf(monthOfYear + 1)+year;
        }
        switch (mWhichDate){
            case R.id.start_date_picker:
                mStartDay.setText(selected_day);
                mData.setStartDate(custom_day);
                break;
            case R.id.end_date_picker:
                mEndDay.setText(selected_day);
                mData.setEndDate(custom_day);
                break;
            default:
                break;
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second){
        String selected_time = hourOfDay+":"+minute;
        String custom_time = String.valueOf((hourOfDay - 5) * 60 + minute);
        switch (mWhichTime){
            case R.id.start_time_picker:
                mStartTime.setText(selected_time);
                mData.setStartTime(custom_time);
                break;
            case R.id.end_time_picker:
                mEndTime.setText(selected_time);
                mData.setEndTime(custom_time);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mData.getStartDate()!=null) outState.putString("start_day", mData.getStartDate());
        if (mData.getEndDate()!=null) outState.putString("end_day", mData.getEndDate());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mData.setStartDate(savedInstanceState.getString("start_day"));
        mData.setEndDate(savedInstanceState.getString("end_day"));
    }
}
