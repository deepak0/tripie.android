package com.tripie.app.tripie;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import model.Data;
import model.adapters.InstructionsAdapter;

/**
 * Created by deepak on 20/12/15.
 */
public class InstructionsActivity extends FragmentActivity {
    private ViewPager mViewPager;
    private InstructionsAdapter mAdapter;
    private SharedPreferences mSharedPreferences;
    private Button btn1, btn2, btn3, btn4, btn5;
    private Data mData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);

        mSharedPreferences = this.getSharedPreferences(
                getString(R.string.tripie_sign_up_key), Context.MODE_PRIVATE);
        String sign_up_key = mSharedPreferences.getString(getString(R.string.tripie_sign_up_key), null);

        if (sign_up_key==null){
            setContentView(R.layout.instructions_container);
            /**
             * Custom Action Bar */
            ActionBar mActionBar = getActionBar();
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
            LayoutInflater mInflater = LayoutInflater.from(this);
            View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mActionBar.setCustomView(mCustomView, layoutParams);
            mActionBar.setDisplayShowCustomEnabled(true);

            mData = Data.getInstance(this);
            MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, mData.getPROJECT_TOKEN());
            String android_id = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            mData.setDeviceId(android_id);

            try {
                JSONObject props = new JSONObject();
                props.put("DeviceId", android_id);
                props.put("Activity", "Instructions");
                mixpanel.track("App Downloaded - SignUp", props);
            } catch (JSONException e) {
                Log.e("MIXPANEL", "Unable to add properties to JSONObject", e);
            }

            mViewPager = (ViewPager) findViewById(R.id.viewPager);
            mAdapter = new InstructionsAdapter(getSupportFragmentManager());
            mViewPager.setAdapter(mAdapter);
            mViewPager.addOnPageChangeListener(mPager);
            mViewPager.setCurrentItem(0);
            initButton();
        } else {
            Intent i = new Intent(this, BasicInfoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    private void initButton(){
        btn1 = (Button)findViewById(R.id.btn1);
        btn2 = (Button)findViewById(R.id.btn2);
        btn3 = (Button)findViewById(R.id.btn3);
        btn4 = (Button)findViewById(R.id.btn4);
        btn5 = (Button)findViewById(R.id.btn5);
        setButton(btn1,"1",40,40);
        setButton(btn2,"",20,20);
        setButton(btn3,"",20,20);
        setButton(btn4,"",20,20);
        setButton(btn5,"",20,20);
    }

    private void btnAction(int action){
        switch(action){
            case 0:
                setButton(btn1,"1",40,40);
                setButton(btn2,"",20,20);
                setButton(btn3,"",20,20);
                setButton(btn4,"",20,20);
                setButton(btn5,"",20,20);
                break;
            case 1:
                setButton(btn2,"2",40,40);
                setButton(btn1,"",20,20);
                setButton(btn3,"",20,20);
                setButton(btn4,"",20,20);
                setButton(btn5,"",20,20);
                break;
            case 2:
                setButton(btn3,"3",40,40);
                setButton(btn1,"",20,20);
                setButton(btn2,"",20,20);
                setButton(btn4,"",20,20);
                setButton(btn5,"",20,20);
                break;
            case 3:
                setButton(btn4,"4",40,40);
                setButton(btn1,"",20,20);
                setButton(btn2,"",20,20);
                setButton(btn3,"",20,20);
                setButton(btn5,"",20,20);
                break;
            case 4:
                setButton(btn5,"5",40,40);
                setButton(btn1,"",20,20);
                setButton(btn2,"",20,20);
                setButton(btn4,"",20,20);
                setButton(btn3,"",20,20);
                break;
            default:
                break;
        }
    }

    private void setButton(Button btn, String text, int h, int w){
        btn.setWidth(w);
        btn.setHeight(h);
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk<16) {
            //Deprecated code for API level 15 or below.
            if (text.equals("")) btn.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rounded_cell));
            else btn.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rounded_cell_selected));
        } else {
            if (text.equals("")) btn.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_cell));
            else btn.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_cell_selected));
        }
    }

    private ViewPager.OnPageChangeListener mPager = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        @Override
        public void onPageSelected(int position) {
            btnAction(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {}
    };
}
