package com.tripie.app.tripie;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Data;
import model.ItineraryPOJO.Day;
import model.ItineraryPOJO.Location;
import model.ItineraryPOJO.Main;
import model.adapters.ListAdapter;

/**
 * Created by deepak on 6/10/15.
 */
public class ItineraryActivity extends Activity{
    private Context mContext;
    private ImageButton mMapButton;
    private ListView mListView;
    private Data mData;
    private String mJSON;
    private Spinner mSpinner;
    private List<Day> mDaysList;
    private List<Location> mLocationList;
    private List<MapCoordinates> mLocLatLng;
    private ImageButton mRefreshButton;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private AlertDialog.Builder mDialog;
    private final String BASE_API = "http://tripie.in/api";

    public String getFormattedTime(int time){
        String f_time;
        int hours = time/60;
        int min = time - (hours * 60);
        hours = hours + 5;
        String suffix = "AM";
        if (hours > 12){
            suffix = "PM";
            hours = hours - 12;
        }
        f_time = hours+":"+min+" "+suffix;
        if (min<10){
            f_time = hours+":0"+min+" "+suffix;
        }
        return f_time;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itinerary);
        /**
         * Custom Action Bar */
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);
        mActionBar.setDisplayShowCustomEnabled(true);

        mSharedPreferences = this.getSharedPreferences(
                getString(R.string.tripie_itinerary_url), Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();

        mData = Data.getInstance(this);
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, mData.getPROJECT_TOKEN());

        try {
            JSONObject props = new JSONObject();
            props.put("DeviceId", mData.getDeviceId());
            props.put("Activity", "Itinerary");
            mixpanel.track("Itinerary Created", props);
        } catch (JSONException e) {
            Log.e("MIXPANEL", "Unable to add properties to JSONObject", e);
        }

        mContext = this;
        mLocLatLng = new ArrayList<>();
        List<String> mSpinnerDays = new ArrayList<>();
        mSpinner = (Spinner)findViewById(R.id.dayForMap);
        mRefreshButton = (ImageButton)findViewById(R.id.refresh_button);

        if (savedInstanceState!=null && savedInstanceState.getSerializable("json_data")!=null){
            mJSON = (String)savedInstanceState.getSerializable("json_data");
        } else {
            mJSON = mData.getJson();
        }
        Gson gson = new Gson();
        Main mMain = gson.fromJson(mJSON, Main.class);
        mDaysList = mMain.getPlan().getDayList();
        int lenDaysList = mDaysList.size();

        for (int i=0; i<lenDaysList; i++){
            String mDay = "Day "+(i+1);
            mSpinnerDays.add(mDay);
        }

        mMapButton = (ImageButton)findViewById(R.id.goto_map_icon);
        mListView = (ListView)findViewById(R.id.schedule_list);
        ArrayAdapter<String> mSpinnerAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, mSpinnerDays);
        mSpinner.setAdapter(mSpinnerAdapter);
        mSpinner.setOnItemSelectedListener(mDayListener);
        mRefreshButton.setOnClickListener(mRefreshListener);
        mMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ItineraryActivity.this, MapActivity.class);
                startActivity(i);
            }
        });
    }

    private AdapterView.OnItemSelectedListener mDayListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            repopulateList(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void repopulateList(int position){
        ListAdapter listAdapter = new ListAdapter(this, getDayLocList(position));
        mListView.setAdapter(listAdapter);
    }

    private List<String[]> getDayLocList(int position){
        List<String[]> list = new ArrayList<>();
        mLocationList = mDaysList.get(position).getLocationList();
        for (int j=0; j<mLocationList.size(); j++) {
            String id = mLocationList.get(j).getId();
            int start_time = Math.round(Float.parseFloat(mLocationList.get(j).getStartTime()));
            int end_time = Math.round(Float.parseFloat(mLocationList.get(j).getEndTime()));
            String visit_time = getFormattedTime(start_time)+" - "+getFormattedTime(end_time);
            list.add(new String[] {
                    mLocationList.get(j).getLocName(),
                    visit_time,
                    id,
                    mLocationList.get(j).getLatitude(),
                    mLocationList.get(j).getLongitude(),
                    mLocationList.get(j).getDescription(),
                    mLocationList.get(j).getComment(),
                    mLocationList.get(j).getTimings(),
                    mLocationList.get(j).getCommentExtra()});
            mLocLatLng.add(new MapCoordinates(
                    mLocationList.get(j).getLatitude(),
                    mLocationList.get(j).getLongitude()));
        }
        mData.setLocLatLng(mLocLatLng);
        return list;
    }

    private void setVisitedPlaces(){
        HashMap<Object, Object> hashMap = mData.getVisited();
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : hashMap.entrySet()){
            if (entry.getValue().equals("1")) {
                sb.append(entry.getKey());
                sb.append(",");
            }
        }
        String s = sb.toString();
        if (s.length()>0 && String.valueOf(s.charAt(s.length() - 1)).equals(",")){
            s = s.substring(0, s.length()-1);
        }
        //hashMap.clear();
        //mData.setVisited(hashMap);
        if (s.length()==0) s="100";
        mData.setVisitedPlaces(s);
        String url = makeItineraryURL();
        mEditor.putString(getString(R.string.tripie_itinerary_url), url);
        mEditor.commit();
    }

    private View.OnClickListener mRefreshListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisitedPlaces();
            String url = mSharedPreferences.getString(getString(R.string.tripie_itinerary_url), "null");
            AsyncTask asyncTask = new ItineraryRequest();
            asyncTask.execute(url);
        }
    };

    private String makeItineraryURL(){
        StringBuilder itineraryURL = new StringBuilder();
        itineraryURL.append("http://tripie.in/api/Plan/");
        itineraryURL.append("?City=");
        itineraryURL.append(mData.getLocation());
        itineraryURL.append("&deviceId=");
        itineraryURL.append(mData.getDeviceId());
        itineraryURL.append("&StartTime=");
        itineraryURL.append(mData.getStartTime());
        itineraryURL.append("&EndTime=");
        itineraryURL.append(mData.getEndTime());
        itineraryURL.append("&StartDate=");
        itineraryURL.append(mData.getStartDate());
        itineraryURL.append("&EndDate=");
        itineraryURL.append(mData.getEndDate());
        itineraryURL.append("&Visited=");
        itineraryURL.append(mData.getVisitedPlaces());
        itineraryURL.append("&Interest=");
        itineraryURL.append(mData.getInterests());
        itineraryURL.append("&StartLocation=");
        itineraryURL.append(mData.getStartingPoint());
        itineraryURL.append("&StayLocation=");
        itineraryURL.append(mData.getStayLocation());
        return itineraryURL.toString();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("json_data", mJSON);
        outState.putString("city", mData.getLocation());
        outState.putString("device_id", mData.getDeviceId());
        outState.putString("start_time", mData.getStartTime());
        outState.putString("end_time", mData.getEndTime());
        outState.putString("start_day", mData.getStartDate());
        outState.putString("end_day", mData.getEndDate());
        outState.putString("visited_places", mData.getVisitedPlaces());
        outState.putString("interests", mData.getInterests());
        outState.putString("starting_point", mData.getStartingPoint());
        outState.putString("stay_location", mData.getStayLocation());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("BUNDLE", "Inside onRestoreInstanceState");
        mJSON = (String)savedInstanceState.getSerializable("json_data");
        mData.setLocation(savedInstanceState.getString("city"));
        mData.setDeviceId(savedInstanceState.getString("device_id"));
        mData.setStartTime(savedInstanceState.getString("start_time"));
        mData.setEndTime(savedInstanceState.getString("end_time"));
        mData.setStartDate(savedInstanceState.getString("start_day"));
        mData.setEndDate(savedInstanceState.getString("end_day"));
        mData.setVisitedPlaces(savedInstanceState.getString("visited_places"));
        mData.setInterests(savedInstanceState.getString("interests"));
        mData.setStartingPoint(savedInstanceState.getString("starting_point"));
        mData.setStayLocation(savedInstanceState.getString("stay_location"));
    }

    @Override
    public void onBackPressed() {
        mDialog = new AlertDialog.Builder(this);
        mDialog.setTitle("Proceed");
        mDialog.setMessage("This will delete your Itinerary");
        mDialog.setPositiveButton("Proceed",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        HashMap hashMap = mData.getVisited();
                        hashMap.clear();
                        mData.setVisited(hashMap);
                        mData.setVisitedPlaces("100");
                        finish();
                    }
                });
        mDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        mDialog.show();
    }

    public class ItineraryRequest extends AsyncTask {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(mContext);
            pd.setMessage("Creating Itinerary");
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }

        @Override
        protected Object doInBackground(Object... params) {
            try {
                JSONParser jParser = new JSONParser();
                return jParser.getJSONFromUrl(params[0].toString());
            }catch (IOException e){
                Log.e("JSON_ERROR", "Unable to retrieve content", e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object json) {
            try {
                pd.hide();
                pd.dismiss();
                if (json.toString()!=null && !json.toString().isEmpty()){
                    mData.setJson(json.toString());
                    Intent i = new Intent(getApplicationContext(), ItineraryActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Internet Connection Error", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e("JSON_ERROR", "Could not parse malformed JSON");
                Toast.makeText(getApplicationContext(), "Internet Connection Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
