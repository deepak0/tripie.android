package com.tripie.app.tripie;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import model.Data;

/**
 * Created by deepak on 12/17/15.
 */
public class ItineraryDetails extends Activity{
    private TextView mName;
    private TextView mDescription;
    private TextView mTimings;
    private TextView mComment;
    private TextView mCommentExtra;
    private ImageButton mNavigate;
    private ImageView mDetailsImage;
    private Data mData;
    private String lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.itinerary_details);
        /**
         * Custom Action Bar */
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);
        mActionBar.setDisplayShowCustomEnabled(true);

        mData = Data.getInstance(this);

        Intent i = getIntent();
        String id = i.getStringExtra("id");
        String name = i.getStringExtra("name");
        lat = i.getStringExtra("lat");
        lng = i.getStringExtra("lng");
        String comment = i.getStringExtra("comment");
        String timing = i.getStringExtra("timing");
        String description = i.getStringExtra("description");
        String comment_extra = i.getStringExtra("comment_extra");

        mName = (TextView)findViewById(R.id.place_name);
        mDescription = (TextView)findViewById(R.id.place_description);
        mTimings = (TextView)findViewById(R.id.place_timings);
        mComment = (TextView)findViewById(R.id.place_comments);
        mCommentExtra = (TextView)findViewById(R.id.place_comments_extra);
        mDetailsImage = (ImageView)findViewById(R.id.details_image_view);
        mNavigate = (ImageButton)findViewById(R.id.navigate);
        mNavigate.setOnClickListener(mNavigateListener);
        mName.setText(name);
        mDescription.setText(description);
        mComment.setText(comment);
        mCommentExtra.setText(comment_extra);
        mTimings.setText(timing);
        int img_id = getResources().getIdentifier("d"+id, "drawable", getPackageName());
        mDetailsImage.setImageResource(img_id);
    }

    private View.OnClickListener mNavigateListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String c_lat = mData.getCurrentLocLat();
            String c_lng = mData.getCurrentLocLng();
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + c_lat + "," + c_lng + "&daddr=" + lat + "," + lng));
            intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
            startActivity(intent);
        }
    };
}
