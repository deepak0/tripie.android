package com.tripie.app.tripie;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by deepak on 19/9/15.
 */
public class JSONParser {

    public JSONParser() {
    }

    public String getJSONFromUrl(String url) throws IOException {

        InputStream is = null;
        try {
            URL url1 = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url1.openConnection();
            //httpURLConnection.setReadTimeout(10000);
            //httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            int response = httpURLConnection.getResponseCode();
            Log.d("DEBUG_TAG", "The response is: " + response);
            is = httpURLConnection.getInputStream();
            String inputcontentstream = readIt(is);
            httpURLConnection.disconnect();
            return inputcontentstream;
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream is) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        String json = sb.toString();
        return json;
    }
}