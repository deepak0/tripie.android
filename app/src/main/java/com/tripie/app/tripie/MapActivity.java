package com.tripie.app.tripie;

import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.Data;

/**
 * Created by deepak on 16/9/15.
 */
public class MapActivity extends Activity implements OnMapReadyCallback{

    private List<MapCoordinates> mPlacesToVisit;
    private Data mData;
    private List<String> mDirectionURL;
    // Functions for google maps directions
//    public String makeMapDirectionsURL (double sourcelat, double sourcelog, double destlat, double destlog){
//        StringBuilder urlString = new StringBuilder();
//        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
//        urlString.append("?origin=");// from
//        urlString.append(Double.toString(sourcelat));
//        urlString.append(",");
//        urlString.append(Double.toString(sourcelog));
//        urlString.append("&destination=");// to
//        urlString.append(Double.toString(destlat));
//        urlString.append(",");
//        urlString.append(Double.toString(destlog));
//        urlString.append("&sensor=false&mode=driving&alternatives=true");
//        urlString.append("&key=AIzaSyCnpPqGVrmmCR0uzjXZUVeOTy2tatpG1_k");
//        return urlString.toString();
//    }

    public String makeWaypointsDirectionsURL (List<MapCoordinates> waypointsArray){
        int locations = waypointsArray.size();
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");
        urlString.append(waypointsArray.get(0).getLatitude());
        urlString.append(",");
        urlString.append(waypointsArray.get(0).getLongitude());
        urlString.append("&destination=");
        urlString.append(waypointsArray.get(locations-1).getLatitude());
        urlString.append(",");
        urlString.append(waypointsArray.get(locations-1).getLongitude());
        urlString.append("&waypoints=");
        for (int i=1; i<(locations-1); i++){
            if (i==7) break;
            urlString.append(waypointsArray.get(i).getLatitude());
            urlString.append(",");
            urlString.append(waypointsArray.get(i).getLongitude());
            urlString.append("|");
        }
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&key=AIzaSyCnpPqGVrmmCR0uzjXZUVeOTy2tatpG1_k");
        return urlString.toString();
    }

    public void drawPath(String  result, GoogleMap myMap, String color) {

        try {
            //Tranform the string into a json object
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            Polyline line = myMap.addPolyline(new PolylineOptions()
                            .addAll(list)
                            .width(12)
                            .color(Color.parseColor(color))
                            .geodesic(true)
            );
           /*
           for(int z = 0; z<list.size()-1;z++){
                LatLng src= list.get(z);
                LatLng dest= list.get(z+1);
                Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude,   dest.longitude))
                .width(2)
                .color(Color.BLUE).geodesic(true));
            }
           */
        }
        catch (JSONException e) {

        }
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        /**
         * Custom Action Bar */
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);
        mActionBar.setDisplayShowCustomEnabled(true);

        mData = Data.getInstance(this);
        MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap map){
        mPlacesToVisit = mData.getLocLatLng();
        map.clear();

        for (int i=0; i<mPlacesToVisit.size(); i++){
            map.addMarker(new MarkerOptions()
                .position(new LatLng(
                        Float.parseFloat(mPlacesToVisit.get(i).getLatitude()),
                        Float.parseFloat(mPlacesToVisit.get(i).getLongitude())))
                .title("Destination "+(i+1)));
        }

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        Float.parseFloat(mPlacesToVisit.get(0).getLatitude()),
                        Float.parseFloat(mPlacesToVisit.get(0).getLongitude())),
                11));
        map.setMyLocationEnabled(true);

//        mDirectionURL = new ArrayList<String>();
//        for (int i=0; i<(mPlacesToVisit.size()-1); i++) {
//            String url = makeMapDirectionsURL(
//                    mPlacesToVisit.get(i).getLatitude(),
//                    mPlacesToVisit.get(i).getLongitude(),
//                    mPlacesToVisit.get(i+1).getLatitude(),
//                    mPlacesToVisit.get(i+1).getLongitude());
//            mDirectionURL.add(url);
//        }

        String url = makeWaypointsDirectionsURL(mPlacesToVisit);

        AsyncTask<String,Void,String> asyncTask = new AsyncTask<String,Void,String>(){
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONParser jParser = new JSONParser();
                    String json = jParser.getJSONFromUrl(params[0]);
                    return json;
                }catch (IOException e){
                    return "Unable to retrieve web page. URL may be invalid.";
                }
            }

            @Override
            protected void onPostExecute(String json) {
                List<String> mColor = new ArrayList<>();
                mColor.add("#05b1fb");
                drawPath(json, map, mColor.get(0));
            }
        };
        asyncTask.execute(url);
    }

}
