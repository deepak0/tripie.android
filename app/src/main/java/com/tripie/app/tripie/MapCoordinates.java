package com.tripie.app.tripie;


/**
 * Created by deepak on 19/9/15.
 */
public class MapCoordinates {
    private String mLatitude;
    private String mLongitude;

    public MapCoordinates(String Latitude, String Longitude){
        this.mLatitude = Latitude;
        this.mLongitude = Longitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }
}
