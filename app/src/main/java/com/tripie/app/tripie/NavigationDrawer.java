package com.tripie.app.tripie;

import android.app.ActionBar;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by deepak on 3/9/15.
 */
public class NavigationDrawer extends FragmentActivity {
    private DrawerLayout mDrawerLayout;
    private String[] mDrawerListTitles;
    private ListView mDrawerListView;

    @Override
    public void onCreate(Bundle savedInstanceState){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_drawer);
        /**
         * Custom Action Bar */
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);
        mActionBar.setDisplayShowCustomEnabled(true);
        /**
         * Custom Navigation Drawer */
        mDrawerLayout = (DrawerLayout)findViewById(R.id.layout_drawer);
        mDrawerListTitles = getResources().getStringArray(R.array.drawer_items);
        mDrawerListView = (ListView)findViewById(R.id.left_drawer);
        mDrawerListView.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mDrawerListTitles));

//        FragmentManager fragmentManager = getFragmentManager();
//        Fragment activity_fragment = fragmentManager.findFragmentById(R.id.content_frameContainer);
//        if (activity_fragment==null){
//            activity_fragment = new BasicInfoActivity();
//        }
//        Fragment itinerary_fragment = fragmentManager.findFragmentByTag("Itinerary");
//        if (itinerary_fragment==null){
//            itinerary_fragment = new ItineraryActivity();
//        }
//        fragmentManager.beginTransaction()
//                .add(R.id.content_frameContainer, activity_fragment)
//                .commit();
//        View.OnKeyListener mBackKeyListener = new View.OnKeyListener()
//        {
//            @Override
//            public boolean onKey( View v, int keyCode, KeyEvent event )
//            {
//                if( keyCode == KeyEvent.KEYCODE_BACK )
//                {
//                    Log.d("Back", "Back Key Pressed");
//                    return true;
//                }
//                return false;
//            }
//        };
//
//        itinerary_fragment.getView().setFocusableInTouchMode(true);
//        itinerary_fragment.getView().requestFocus();
//        itinerary_fragment.getView().setOnKeyListener(mBackKeyListener);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
