package com.tripie.app.tripie;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import API.TripieAPI;
import model.AppLocationService;
import model.Data;
import model.DelayAutoCompleteTextView;
import model.GeoSearchResult;
import model.ItineraryPOJO.Main;
import model.NAWrapper;
import model.adapters.GeoAutoCompleteAdapter;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by deepak on 12/9/15.
 */
public class PlanActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private Integer THRESHOLD = 2;
    private Context mContext;
    private DelayAutoCompleteTextView geo_autocomplete;
    private ImageView geo_autocomplete_clear;
    private Data mData;
    private Button mPlan;
    private String requestURL;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private String mCurrentLatitude = "";
    private String mNetworkLat;
    private String mNetworkLng;
    private String mCurrentLongitude = "";
    private String mSelectedLat = "";
    private String mSelectedLng = "";
    private Geocoder gc;
    private NAWrapper mWrapper;
    private SharedPreferences.Editor mEditor;
    private String mStoredURL;
    private AppLocationService mLocationService;
    private Location mNetworkLocation;
    private static final String BASE_URL = "http://tripie.in";
    private TripieAPI mAPI;
    private ProgressDialog pd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        /**
         * Custom Action Bar */
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);
        mActionBar.setDisplayShowCustomEnabled(true);

        /**
         * Create an instance of GoogleAPIClient */
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mData = Data.getInstance(this);
        mContext = this;
        gc = new Geocoder(this);

        mLocationService = new AppLocationService(this);
        fetchNetworkLocation();

        SharedPreferences sharedPreferences = this.getSharedPreferences(
                getString(R.string.tripie_itinerary_url), Context.MODE_PRIVATE);
        mEditor = sharedPreferences.edit();
        mStoredURL = sharedPreferences.getString(getString(R.string.tripie_itinerary_url), "null");
        Log.d("STORAGE", mStoredURL);

        pd = new ProgressDialog(mContext);
        pd.setMessage("Creating Itinerary");
        pd.setCanceledOnTouchOutside(false);

        HashMap hashMap = mData.getVisited();
        hashMap.clear();
        mData.setVisited(hashMap);

        mPlan = (Button)findViewById(R.id.plan_button);

        mWrapper = (NAWrapper)findViewById(R.id.location_wrapper);

        geo_autocomplete_clear = (ImageView) findViewById(R.id.geo_autocomplete_clear);
        geo_autocomplete = (DelayAutoCompleteTextView) findViewById(R.id.geo_autocomplete);
        geo_autocomplete.setThreshold(THRESHOLD);
        geo_autocomplete.setAdapter(new GeoAutoCompleteAdapter(this));
        geo_autocomplete.setOnItemClickListener(mAddressListener);

        geo_autocomplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    geo_autocomplete_clear.setVisibility(View.VISIBLE);
                } else {
                    geo_autocomplete_clear.setVisibility(View.GONE);
                }
            }
        });

        geo_autocomplete_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                geo_autocomplete.setText("");
            }
        });

        mPlan.setOnClickListener(mPlanListener);
    }

    private View.OnClickListener mPlanListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            fetchNetworkLocation();
            String starting_point = getStartingPoint();
            if (starting_point.equals(",")){
                Toast.makeText(PlanActivity.this, "Enter Your Location", Toast.LENGTH_SHORT).show();
                Log.e("LOC_ERROR", starting_point);
            } else {
                pd.show();
                requestURL = makeItineraryURL();
                mEditor.putString(getString(R.string.tripie_itinerary_url), requestURL);
                mEditor.commit();
                OkHttpClient okHttpClient = new OkHttpClient();
                okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
                okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                mAPI = retrofit.create(TripieAPI.class);
                Call<Main> mainCall = mAPI.getItinerary(
                        mData.getLocation(),
                        mData.getDeviceId(),
                        mData.getStartTime(),
                        mData.getEndTime(),
                        mData.getStartDate(),
                        mData.getEndDate(),
                        mData.getVisitedPlaces(),
                        mData.getInterests(),
                        mData.getStartingPoint(),
                        mData.getStayLocation());
                mainCall.enqueue(mMainCallback);
            }
        }
    };

    private Callback<Main> mMainCallback = new Callback<Main>() {
        @Override
        public void onResponse(Response<Main> response, Retrofit retrofit) {
            Main main = response.body();
            Log.d("Retrofit", "Itinerary Status Code"+response.code());
            Gson mainGson = new Gson();
            String convertedData = mainGson.toJson(main);
            pd.hide();
            pd.dismiss();
            if (convertedData!=null && !convertedData.isEmpty()){
                mData.setJson(convertedData);
                Intent i = new Intent(mContext, ItineraryActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(mContext, "Internet Connection Error", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Throwable t) {
            pd.hide();
            Log.d("Retrofit", "Unable to fetch Itinerary data", t);
            Toast.makeText(mContext, "Internet Connection Error", Toast.LENGTH_SHORT).show();
        }
    };

    private AdapterView.OnItemClickListener mAddressListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            GeoSearchResult result = (GeoSearchResult) adapterView.getItemAtPosition(position);
            geo_autocomplete.setText(result.getAddress());
            String input = result.getAddress();
            try {
                List<Address> list = gc.getFromLocationName(input, 1);
                Address address = list.get(0);
                mSelectedLat = String.valueOf(address.getLatitude());
                mSelectedLng = String.valueOf(address.getLongitude());
            } catch (IOException e){
                Log.e("LOC", "Starting Point Loc Fetch Error");
            }
        }
    };

    private String getStartingPoint(){
        String starting_point;
        if (mWrapper.isChecked()){
            starting_point = mNetworkLat+","+mNetworkLng;
        } else {
            starting_point = mSelectedLat+","+mSelectedLng;
        }
        mData.setStartingPoint(starting_point);
        return starting_point;
    }

    private String getStayLocation(){
        String stay_loc;
        if (mSelectedLng.equals("") || mSelectedLat.equals("")){
            stay_loc = mNetworkLat+","+mNetworkLng;
        } else {
            stay_loc = mSelectedLat+","+mSelectedLng;
        }
        mData.setStayLocation(stay_loc);
        return stay_loc;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            mCurrentLatitude = String.valueOf(mLastLocation.getLatitude());
            mCurrentLongitude = String.valueOf(mLastLocation.getLongitude());
        }
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("LOC", "Failed to load Current Location");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("city", mData.getLocation());
        outState.putString("device_id", mData.getDeviceId());
        outState.putString("start_time", mData.getStartTime());
        outState.putString("end_time", mData.getEndTime());
        outState.putString("start_day", mData.getStartDate());
        outState.putString("end_day", mData.getEndDate());
        outState.putString("interests", mData.getInterests());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mData.setLocation(savedInstanceState.getString("city"));
        mData.setDeviceId(savedInstanceState.getString("device_id"));
        mData.setStartTime(savedInstanceState.getString("start_time"));
        mData.setEndTime(savedInstanceState.getString("end_time"));
        mData.setStartDate(savedInstanceState.getString("start_day"));
        mData.setEndDate(savedInstanceState.getString("end_day"));
        mData.setInterests(savedInstanceState.getString("interests"));
    }

    public void showSettingsAlert(String provider) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(provider + " Settings");
        alertDialog.setMessage(provider + " Location is not enabled! Go to settings menu");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        PlanActivity.this.startActivityForResult(intent, 0);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    private void fetchNetworkLocation(){
        mNetworkLocation = mLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
        if (mNetworkLocation!=null){
            mNetworkLat = String.valueOf(mNetworkLocation.getLatitude());
            mNetworkLng = String.valueOf(mNetworkLocation.getLongitude());
            mData.setCurrentLocLat(mNetworkLat);
            mData.setCurrentLocLng(mNetworkLng);
        } else {
            showSettingsAlert("Network");
        }
    }

    private String makeItineraryURL(){
        StringBuilder itineraryURL = new StringBuilder();
        itineraryURL.append("http://tripie.in/api/Plan/");
        itineraryURL.append("?City=");
        itineraryURL.append(mData.getLocation());
        itineraryURL.append("&deviceId=");
        itineraryURL.append(mData.getDeviceId());
        itineraryURL.append("&StartTime=");
        itineraryURL.append(mData.getStartTime());
        itineraryURL.append("&EndTime=");
        itineraryURL.append(mData.getEndTime());
        itineraryURL.append("&StartDate=");
        itineraryURL.append(mData.getStartDate());
        itineraryURL.append("&EndDate=");
        itineraryURL.append(mData.getEndDate());
        itineraryURL.append("&Visited=");
        itineraryURL.append(mData.getVisitedPlaces());
        itineraryURL.append("&Interest=");
        itineraryURL.append(mData.getInterests());
        itineraryURL.append("&StartLocation=");
        itineraryURL.append(getStartingPoint());
        itineraryURL.append("&StayLocation=");
        itineraryURL.append(getStayLocation());
        return itineraryURL.toString();
    }

}
