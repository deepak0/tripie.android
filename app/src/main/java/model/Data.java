package model;

import android.content.Context;

import com.tripie.app.tripie.MapCoordinates;

import java.util.HashMap;
import java.util.List;


/**
 * Created by deepak on 9/10/15.
 */
public class Data {
    private Context mContext;
    private static Data mData;
    private String mDeviceId;
    private String mLocation;
    private String mStartTime = "240";
    private String mEndTime = "1080";
    private String mJson;
    private List<MapCoordinates> mLocLatLng;
    private String mCurrentLocLat;
    private String mCurrentLocLng;
    private String mInterests;
    private String mStartDate;
    private String mEndDate;
    private String mStartingPoint;
    private String mStayLocation;
    private String mVisitedPlaces = "100";
    private HashMap mVisited = new HashMap();
    private String PROJECT_TOKEN = "ab67347a9c42fabad3343d630c8ade4c";

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String mLocation) {
        this.mLocation = mLocation;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public void setStartTime(String mStartTime) {
        this.mStartTime = mStartTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public void setEndTime(String mEndTime) {
        this.mEndTime = mEndTime;
    }

    public String getJson() {
        return mJson;
    }

    public void setJson(String json) {
        mJson = json;
    }

    public List<MapCoordinates> getLocLatLng() {
        return mLocLatLng;
    }

    public void setLocLatLng(List<MapCoordinates> locLatLng) {
        mLocLatLng = locLatLng;
    }

    public String getCurrentLocLat() {
        return mCurrentLocLat;
    }

    public void setCurrentLocLat(String currentLocLat) {
        mCurrentLocLat = currentLocLat;
    }

    public String getCurrentLocLng() {
        return mCurrentLocLng;
    }

    public void setCurrentLocLng(String currentLocLng) {
        mCurrentLocLng = currentLocLng;
    }

    public String getInterests() {
        return mInterests;
    }

    public void setInterests(String interests) {
        mInterests = interests;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String startDate) {
        mStartDate = startDate;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String endDate) {
        mEndDate = endDate;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public HashMap getVisited() {
        return mVisited;
    }

    public void setVisited(HashMap visited) {
        mVisited = visited;
    }

    public String getVisitedPlaces() {
        return mVisitedPlaces;
    }

    public void setVisitedPlaces(String visitedPlaces) {
        mVisitedPlaces = visitedPlaces;
    }

    public String getStartingPoint() {
        return mStartingPoint;
    }

    public void setStartingPoint(String startingPoint) {
        mStartingPoint = startingPoint;
    }

    public String getStayLocation() {
        return mStayLocation;
    }

    public void setStayLocation(String stayLocation) {
        mStayLocation = stayLocation;
    }

    public String getPROJECT_TOKEN() {
        return PROJECT_TOKEN;
    }

    private Data(Context c){
        mContext = c;
    }

    public static Data getInstance(Context c){
        if(mData == null){
            mData = new Data(c);
        }
        return mData;
    }

}
