package model.ItineraryPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepak on 11/10/15.
 */
public class Day {
    @Expose
    @SerializedName("location")
    private List<Location> mLocationList = new ArrayList<>();

    public List<Location> getLocationList() {
        return mLocationList;
    }

    public void setLocationList(List<Location> locationList) {
        mLocationList = locationList;
    }
}
