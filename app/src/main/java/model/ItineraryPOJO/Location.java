package model.ItineraryPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by deepak on 11/10/15.
 */
public class Location {
    @Expose
    @SerializedName("Name")
    private String mLocName;

    @Expose
    @SerializedName("latitude")
    private String mLatitude;

    @Expose
    @SerializedName("longitude")
    private String mLongitude;

    @Expose
    @SerializedName("Start_time")
    private String mStartTime;

    @Expose
    @SerializedName("End_time")
    private String mEndTime;

    @Expose
    @SerializedName("Free_time")
    private String mFreeTime;

    @Expose
    @SerializedName("locationId")
    private String mId;

    @Expose
    @SerializedName("Description")
    private String mDescription;

    @Expose
    @SerializedName("Timings")
    private String mTimings;

    @Expose
    @SerializedName("Comment")
    private String mComment;

    @Expose
    @SerializedName("Comment2")
    private String mCommentExtra;

    public String getLocName() {
        return mLocName;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public String getFreeTime() {
        return mFreeTime;
    }

    public String getId() {
        return mId;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getTimings() {
        return mTimings;
    }

    public String getComment() {
        return mComment;
    }

    public String getCommentExtra() {
        return mCommentExtra;
    }

    public void setLocName(String locName) {
        mLocName = locName;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public void setStartTime(String startTime) {
        mStartTime = startTime;
    }

    public void setEndTime(String endTime) {
        mEndTime = endTime;
    }

    public void setFreeTime(String freeTime) {
        mFreeTime = freeTime;
    }

    public void setId(String id) {
        mId = id;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setTimings(String timings) {
        mTimings = timings;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public void setCommentExtra(String commentExtra) {
        mCommentExtra = commentExtra;
    }
}
