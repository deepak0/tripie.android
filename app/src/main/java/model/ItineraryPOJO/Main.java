package model.ItineraryPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepak on 10/10/15.
 */
public class Main {
    @Expose
    @SerializedName("no_of_days")
    private Integer mNoOfDays;

    @Expose
    @SerializedName("no_of_locations")
    private List<Integer> mNoOfLocations = new ArrayList<>();

    @Expose
    @SerializedName("Plan")
    private Plan mPlan;

    public Integer getNoOfDays() {
        return mNoOfDays;
    }

    public void setNoOfDays(Integer noOfDays) {
        mNoOfDays = noOfDays;
    }

    public List<Integer> getNoOfLocations() {
        return mNoOfLocations;
    }

    public void setNoOfLocations(List<Integer> noOfLocations) {
        mNoOfLocations = noOfLocations;
    }

    public Plan getPlan() {
        return mPlan;
    }

    public void setPlan(Plan plan) {
        mPlan = plan;
    }
}
