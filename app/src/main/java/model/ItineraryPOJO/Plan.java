package model.ItineraryPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepak on 10/10/15.
 */
public class Plan {
    @Expose
    @SerializedName("Day")
    private List<Day> mDayList = new ArrayList<>();

    public List<Day> getDayList() {
        return mDayList;
    }

    public void setDayList(List<Day> dayList) {
        mDayList = dayList;
    }
}
