package model;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tripie.app.tripie.R;

/**
 * Created by deepak on 12/11/15.
 */
public class NAWrapper extends LinearLayout {
    private CheckBox mCheckbox;
    private String mHeading;
    private TextView mHeader;

    public NAWrapper(Context context) {
        this(context, null);
    }

    public NAWrapper(Context context, AttributeSet attrs) {
        super(context, attrs);
        getAttrs(attrs);
        init();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        View header = LayoutInflater.from(getContext()).inflate(R.layout.na_wrapper, this, false);
        addView(header, 0);
        mCheckbox = (CheckBox)findViewById(R.id.na_applicable);
        mHeader = (TextView)findViewById(R.id.na_title);
        mHeader.setText(mHeading);
        mCheckbox.setOnCheckedChangeListener(checkedChangeListener);
    }

    private void getAttrs(AttributeSet attrs){
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NAWrapper);
        try {
            mHeading = a.getString(R.styleable.NAWrapper_heading);
        } finally {
            a.recycle();
        }
    }

    private void init(){
        setOrientation(LinearLayout.VERTICAL);
    }

    private CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked){
                disable();
            } else {
                enable();
            }
        }
    };

    private void enable(){
        for(int i = 1; i < getChildCount(); i++){
            View v = getChildAt(i);
            enableView(v);
            v.setAlpha(1.0f);
        }
    }

    private void disable(){
        for(int i = 1; i < getChildCount(); i++){
            View v = getChildAt(i);
            disableView(v);
            v.setAlpha(0.25f);
        }
    }

    private void enableView(View v){
        v.setEnabled(true);
        if (v instanceof ViewGroup){
            ViewGroup viewGroup = (ViewGroup)v;
            for (int i=0; i<viewGroup.getChildCount(); i++){
                View view = viewGroup.getChildAt(i);
                enableView(view);
            }
        }
    }

    private void disableView(View v){
        v.setEnabled(false);
        if (v instanceof ViewGroup){
            ViewGroup viewGroup = (ViewGroup)v;
            for (int i=0; i<viewGroup.getChildCount(); i++){
                View view = viewGroup.getChildAt(i);
                disableView(view);
            }
        }
    }

    public boolean isChecked(){
        return mCheckbox.isChecked();
    }

}
