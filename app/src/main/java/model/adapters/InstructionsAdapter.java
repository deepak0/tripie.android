package model.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import Fragments.InstructionsFragment;
import Fragments.SignUpInstruction;

/**
 * Created by deepak on 20/12/15.
 */
public class InstructionsAdapter extends FragmentPagerAdapter {
    public static int totalPage = 5;

    public InstructionsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        Bundle args = new Bundle();
        switch(position){
            case 0:
                f = InstructionsFragment.newInstance();
                args.putInt("page", 1);
                f.setArguments(args);
                break;
            case 1:
                f = InstructionsFragment.newInstance();
                args.putInt("page", 2);
                f.setArguments(args);
                break;
            case 2:
                f = InstructionsFragment.newInstance();
                args.putInt("page", 3);
                f.setArguments(args);
                break;
            case 3:
                f = InstructionsFragment.newInstance();
                args.putInt("page", 4);
                f.setArguments(args);
                break;
            case 4:
                f = SignUpInstruction.newInstance();
                break;
            default:
                break;
        }
        return f;
    }

    @Override
    public int getCount() {
        return totalPage;
    }
}
