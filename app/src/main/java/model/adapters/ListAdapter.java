package model.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tripie.app.tripie.ItineraryDetails;
import com.tripie.app.tripie.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import model.Data;

/**
 * Created by deepak on 12/10/15.
 * For list_row.xml
 */
public class ListAdapter extends BaseAdapter{
    private Context mContext;
    private List<String[]> mStrings;
    private static LayoutInflater layoutInflater = null;
    private Data mData;
    private HashMap mVisitMap;

    public ListAdapter(Context context, List<String[]> strings){
        mContext = context;
        mStrings = strings;
        mData = Data.getInstance(context);
        layoutInflater = (LayoutInflater)mContext.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mStrings.get(position);
    }

    @Override
    public int getCount() {
        return mStrings.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        View v = view;
        if (v==null)
            v = layoutInflater.inflate(R.layout.list_row, null);
        RelativeLayout mClickableLayout = (RelativeLayout)v.findViewById(R.id.item_click_layout);
        ImageView mThumbnail = (ImageView)v.findViewById(R.id.thumb_image);
        TextView mName = (TextView)v.findViewById(R.id.location_name);
        TextView mVisitTime = (TextView)v.findViewById(R.id.visit_time);
        ImageButton mDirections = (ImageButton)v.findViewById(R.id.navigate);

        mClickableLayout.setOnClickListener(mLayoutClickListener);
        mDirections.setOnClickListener(mNavigateListener);
        CheckBox mVisited = (CheckBox)v.findViewById(R.id.visited_checkbox);
        mVisited.setOnCheckedChangeListener(null);
        mVisitMap = mData.getVisited();
        String key = mStrings.get(position)[2];
        Boolean check = (mVisitMap.containsKey(key)) ? "1".equals(mVisitMap.get(key).toString()) : false;
        mVisited.setChecked(check);
        mVisited.setOnCheckedChangeListener(mVisitCheckListener);
        String thumb = "t"+key;
        int thumb_id = mContext.getResources().getIdentifier(thumb, "drawable", mContext.getPackageName());
        if (thumb.equals("t33") || thumb.equals("t34")) thumb_id = R.drawable.tripie_logo_white_small;
        mThumbnail.setBackgroundResource(thumb_id);
        mName.setText(mStrings.get(position)[0]);
        mVisitTime.setText(mStrings.get(position)[1]);
        mClickableLayout.setTag(key);
        mVisited.setTag(key);
        mDirections.setTag(key);
        v.setTag(key);
        return v;
    }

    private View.OnClickListener mNavigateListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String tag = v.getTag().toString();
            String lat = "";
            String lng = "";
            for (int i=0; i<mStrings.size(); i++){
                String[] c = mStrings.get(i);
                if (Arrays.asList(c).contains(tag)){
                    lat = c[3];
                    lng = c[4];
                }
            }
            String c_lat = mData.getCurrentLocLat();
            String c_lng = mData.getCurrentLocLng();
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + c_lat + "," + c_lng + "&daddr=" + lat + "," + lng));
            intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
            mContext.startActivity(intent);
        }
    };

    private View.OnClickListener mLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String tag = v.getTag().toString();
            String lat = "";
            String lng = "";
            String name = "";
            String description = "";
            String comment = "";
            String timing = "";
            String comment_extra = "";
            for (int i=0; i<mStrings.size(); i++){
                String[] c = mStrings.get(i);
                if (Arrays.asList(c).contains(tag)){
                    lat = c[3];
                    lng = c[4];
                    name = c[0];
                    description = c[5];
                    comment = c[6];
                    timing = c[7];
                    comment_extra = c[8];
                }
            }
            Intent i = new Intent(mContext, ItineraryDetails.class);
            i.putExtra("id", tag);
            i.putExtra("name", name);
            i.putExtra("lat", lat);
            i.putExtra("lng", lng);
            i.putExtra("description", description);
            i.putExtra("comment", comment);
            i.putExtra("timing", timing);
            i.putExtra("comment_extra", comment_extra);
            mContext.startActivity(i);
        }
    };

    private CompoundButton.OnCheckedChangeListener mVisitCheckListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String tag = buttonView.getTag().toString();
            if (mVisitMap.containsKey(tag)) mVisitMap.remove(tag);
            String value = isChecked ? "1" : "0";
            mVisitMap.put(tag, value);
        }
    };
}
